import XCTest
@testable import JSWrapper

final class JSWrapperTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
		guard let path = Bundle.module.url(forResource: "test", withExtension: "js") else {
			XCTFail()
			return
		}

		guard let wrapper = try? JSWrapper(resourceUrl: path) else {
			XCTFail()
			return
		}

		let sum: Int = try wrapper.sum(11, 22)
		XCTAssertEqual(sum, 33)
    }
	
	func testExample2() async throws {
		// This is an example of a functional test case.
		// Use XCTAssert and related functions to verify your tests produce the correct
		// results.
		guard let path = Bundle.module.url(forResource: "test", withExtension: "js") else {
			XCTFail()
			return
		}

		guard let wrapper = try? JSWrapper(resourceUrl: path) else {
			XCTFail()
			return
		}

		do {
			let value: Int? = try await wrapper.sumPromiseGenerator(1, 2)
			XCTAssertEqual(value, 3)
		} catch {
			XCTFail("Error \(error).")
		}

	}
}
