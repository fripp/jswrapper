function sum(a, b) {
	return a + b;
}
function sumPromiseGenerator(a, b) {
	return Promise.resolve(sum(a, b));
}

function asyncUnroller(prom, callback, ...param) {
	return eval(prom)(...param).then((p) => callback(p));
}
