import JavaScriptCore

public protocol JSExportable {
	func export(context: JSContext) -> JSValue
}

extension Int: JSExportable {
	public func export(context: JSContext) -> JSValue {
		JSValue(object: self, in: context)
	}
}

extension String: JSExportable {
	public func export(context: JSContext) -> JSValue {
		JSValue(object: self, in: context)
	}
}

public enum JSWrapperError: Error {
	case returnTypeCast
}

@dynamicCallable
final class Callable {
	private let context: JSContext
	private let functionName: String

	init(functionName: String, context: JSContext) {
		self.context = context
		self.functionName = functionName
	}

	public func dynamicallyCall<T>(withArguments args: [JSExportable]) throws -> T {
		guard let result = context
				.objectForKeyedSubscript(functionName)
				.call(withArguments: args.map({ $0.export(context: context) }))
				.toObject() as? T else {
					throw JSWrapperError.returnTypeCast
				}

		return result
	}

	public func dynamicallyCall<T>(withArguments args: [JSExportable]) async throws -> T? {
		return try await withUnsafeThrowingContinuation { continuation in
			let callback: @convention(block) (JSValue) -> Void = { data in
				continuation.resume(returning: data.toObject() as? T)
			}

			let funcName = JSValue(object: functionName, in: context)!

			let arguments = [funcName, JSValue(object: callback, in: context)!] + args.map({$0.export(context: context)})
			context.objectForKeyedSubscript("asyncUnroller").call(withArguments: arguments)
		}
	}
}

@dynamicMemberLookup
public final class JSWrapper {
	private let context: JSContext

	public init(resourceUrl: URL) throws {
		let virtualMachine = JSVirtualMachine()
		context = JSContext(virtualMachine: virtualMachine)
		let script = try String(contentsOf: resourceUrl)
		context.evaluateScript(script, withSourceURL: resourceUrl)
		context.setObject(JSLoggerImpl.self, forKeyedSubscript: "logger" as NSString)
		context.exceptionHandler = { context, value in
			print("exception \(String(describing: value))")
		}
	}

	subscript(dynamicMember member: String) -> Callable {
		return Callable(functionName: member, context: context)
	}
}
