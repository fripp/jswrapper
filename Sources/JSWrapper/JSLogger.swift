//
//  File.swift
//  
//
//  Created by feanor on 12/01/22.
//

import Foundation
import JavaScriptCore

@objc protocol JSLogger: JSExport {
	static func log(string: String)
}

class JSLoggerImpl: NSObject, JSLogger {
	@objc class func log(string: String) {
		print(string)
	}
}
